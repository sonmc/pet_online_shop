﻿using OnlineShop.Application.UseCases.User.Crud.Presenter;

namespace OnlineShop.Application.Helpers
{
    public class MappingConfig
    {
        public static void AutoMapperConfig(IServiceCollection services)
        {
            services.AddAutoMapper(typeof(CreateUserMapping));
        }
    }
}
