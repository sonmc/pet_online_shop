﻿using OnlineShop.Utils;
using Microsoft.AspNetCore.Mvc;
using OnlineShop.Services.Base;
using OnlineShop.Application.UseCase;
using OnlineShop.Core.Schemas.Base;

namespace OnlineShop.Application.UseCases.User.GetCurrentUser
{
    [ApiController]
    [Route("api/users")]
    public class GetCurrentUserCtrl : ControllerBase
    {
        GetCurrentUserFlow workFlow;
        public GetCurrentUserCtrl()
        {
            workFlow = new GetCurrentUserFlow(new UnitOfWork());
        }

        [HttpGet("get-current-user", Name = "GetCurentUser_1")]
        public IActionResult GetCurentUser()
        {
            string token = Request.Cookies[JwtUtil.ACCESS_TOKEN];
            if (string.IsNullOrEmpty(token))
            {
                return Unauthorized();
            }
            Response response = workFlow.GetCurrentUser(token);
            if (response.Status == Message.ERROR)
            {
                return Unauthorized();
            }
            var result = GetCurrentUserPresenter.PresentItem((UserSchema)response.Result);

            return Ok(result);
        }
    }
}
